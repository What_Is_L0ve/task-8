﻿using System;

namespace Task__8
{
    public class Program
    {
        static void Main(string[] args)
        {
            Cat[] cat = new Cat[]
            {
                new Cat("Феликс", EnumBreedCat.Abyssinian, 0.5),
                new Cat("Кузя", EnumBreedCat.British, 2.5),
                new Cat("Том", EnumBreedCat.Siberian, 4)
            };
            Dog[] dog = new Dog[]
            {
                new Dog("Хатико", EnumBreedDog.Akita, 1.5),
                new Dog("Лаки", EnumBreedDog.Alabai, 6),
                new Dog("Вольт", EnumBreedDog.Huskies, 4)
            };
            Client[] client = new Client[]
            {
                new Client("Артур", "89123098332", cat[0], dog[0]),
                new Client("Андрей", "89439989321", dog[1]),
                new Client("Алена", "890921343567", cat[1]),
                new Client("Максим", "89325647752", dog[2], cat[2])
            };

            Service service = new Service();

            for (var i = 0; i < client.Length; i++)
            {
                service.CalculatePrice(client[i]);
            }
        }
    }
}
