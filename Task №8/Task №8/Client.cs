﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__8
{
    public class Client
    {
        public string Phone { get; set; }
        public string Name { get; set; }
        public Animal Animal { get; set; }
        public Animal[] Animals { get; set; }
        public Client(string name, string phone, params Animal[] animals)
        {
            Name = name;
            Phone = phone;
            Animals = animals;
        }
        public Client(string name, string phone, Animal animal)
        {
            Name = name;
            Phone = phone;
            Animal = animal;
        }
    }
}
