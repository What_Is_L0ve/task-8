﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__8
{
    public class Service
    {
        private const int PRICE_SERVICE_DOG = 150;
        private const int PRICE_SERVICE_CAT = 100;

        private const int INITIAL_COEFFICIENT_DOG = 70;
        private const int INITIAL_COEFFICIENT_CAT = 50;

        private double _price;
        private static DateTime TimeBuy { get; set; }
        public void CalculatePrice(Client client)
        {
            var random = new Random();
            var queue = random.Next(5, 20);
            TimeBuy = DateTime.UtcNow.AddMinutes(queue);

            if (client.Animals == null)
            {
                Console.WriteLine($"Поступил заказ от {client.Name} на стрижку питомца {client.Animal.Name}");
                if (client.Animal is Dog)
                {
                    var dog = client.Animal as Dog;
                    Console.WriteLine($"Сложность работы: {dog.Coefficient}");
                    _price = PRICE_SERVICE_DOG + INITIAL_COEFFICIENT_DOG * dog.Coefficient;
                    Console.WriteLine($"Стоимость стрижки для собаки: {_price}");
                }
                else
                {
                    var cat = client.Animal as Cat;
                    Console.WriteLine($"Сложность работы: {cat.Coefficient}");
                    _price = PRICE_SERVICE_CAT + INITIAL_COEFFICIENT_CAT * cat.Coefficient;
                    Console.WriteLine($"Стоимость стрижки для кошки: {_price}");
                }
            }
            else
            {
                var sum = new double[client.Animals.Length];
                for (var i = 0; i < client.Animals.Length; i++)
                {
                    Console.WriteLine($"Поступил заказ от {client.Name} на стрижку питомца {client.Animals[i].Name}");
                    if (client.Animals[i] is Dog)
                    {
                        var dog = client.Animals[i] as Dog;
                        Console.WriteLine($"Сложность работы: {dog.Coefficient}");
                        _price = PRICE_SERVICE_DOG + INITIAL_COEFFICIENT_DOG * dog.Coefficient;
                        sum[i] = _price;
                        Console.WriteLine($"Стоимость стрижки для собаки: {_price}");
                    }
                    else
                    {
                        var cat = client.Animals[i] as Cat;
                        Console.WriteLine($"Сложность работы: {cat.Coefficient}");
                        _price = PRICE_SERVICE_CAT + INITIAL_COEFFICIENT_CAT * cat.Coefficient;
                        sum[i] = _price;
                        Console.WriteLine($"Стоимость стрижки для кошки: {_price}");
                    }
                }
                for (var i = 0; i < sum.Length - 1; i++)
                {
                    _price += sum[i];
                }
            }
            Console.WriteLine($"Время завершения работы {TimeBuy}");
            Console.WriteLine($"Сумма к оплате: {_price}\n");
        }
    }
}
