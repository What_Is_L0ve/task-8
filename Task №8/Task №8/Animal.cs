﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__8
{
    public class Animal
    {
        public string Name { get; set; }
        protected Animal(string name)
        {
            Name = name;
        }
    }
}
