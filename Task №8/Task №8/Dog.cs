﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__8
{
    public class Dog : Animal
    {
        public EnumBreedDog BreedDog { get; set; }
        public double Coefficient { get; set; }
        public Dog(string name, EnumBreedDog breedDog, double coefficient)
            : base(name)
        {
            BreedDog = breedDog;
            Coefficient = coefficient;
        }
    }
}
