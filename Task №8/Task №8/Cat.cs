﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__8
{
    public class Cat : Animal
    {
        public EnumBreedCat BreedCat { get; set; }
        public double Coefficient { get; set; }
        public Cat(string name, EnumBreedCat breedCat, double coefficient)
            : base(name)
        {
            BreedCat = breedCat;
            Coefficient = coefficient;
        }
    }
}
